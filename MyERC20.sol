// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function allowance(address sender, address spender) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    event Transfered(address indexed from, address indexed to, uint256 value);
    event Approved(address indexed sender, address indexed spender, uint256 value);
}

contract MyERC20 is IERC20 {
    uint256 totalSupply_ = 100 ether;
    uint256 comission_percents = 2;
    mapping(address => mapping(address=>uint)) avaible_accounts_transfers;
    mapping(address => uint) public balances;

    constructor(){
        balances[msg.sender] = totalSupply_;
    }

    function totalSupply() public view override returns (uint256){
        return totalSupply_;
    }

    function balanceOf(address _account) public view override returns (uint256){
        return balances[_account];
    }

    function allowance(address _sender, address _spender) public view override returns (uint256){
        return avaible_accounts_transfers[_sender][_spender];
    }

    function transfer(address _recipient, uint256 _tokens) public override returns (bool){
        require(balances[msg.sender] >= _tokens, "Insufficient Balance");
        require(_tokens %100 == 0);
        
        balances[msg.sender] = balances[msg.sender]-_tokens;
        balances[_recipient] = balances[_recipient]+_tokens - (_tokens*comission_percents)/100;

        emit Transfered(msg.sender, _recipient, _tokens);
        return true;
    }
    function approve(address _spender, uint256 _tokens) public override returns (bool){
        avaible_accounts_transfers[msg.sender][_spender] = _tokens;
        emit Approved(msg.sender, _spender, _tokens);
        return true;
    }
    function transferFrom(address _sender, address _recipient, uint256 _tokens) public override returns (bool){
        require(_tokens <= balances[_sender], "Insufficient Balance");
        require(_tokens <= avaible_accounts_transfers[_sender][msg.sender], "Not enough tokens approved");

        balances[_sender] = balances[_sender]-_tokens;
        avaible_accounts_transfers[_sender][msg.sender] = avaible_accounts_transfers[_sender][msg.sender]-_tokens;
        balances[_recipient] = balances[_recipient]+_tokens - (_tokens*comission_percents)/100;

        emit Transfered(_sender, _recipient, _tokens);

        return true;

    }
}